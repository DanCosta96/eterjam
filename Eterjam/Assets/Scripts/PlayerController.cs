﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public AgentMovement agent;
    public InputReader input;

    private Interactable interactable;

    public float speed = 1f;
    void Start()
    {

    }

    void Update()
    {
        Move();
        if (interactable != null && input.ActionButton())
        {
            interactable.Interact();
        }
    }

    private void Move()
    {
        Vector3 dir = input.LeftStickYPlane();
        agent.Move(dir * speed * Time.deltaTime);
        if (dir.magnitude > 0.0f)
        {
            transform.rotation = Quaternion.LookRotation(dir, Vector3.up);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Interactable>() != null)
        {
            interactable = other.GetComponent<Interactable>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Interactable>() != null)
        {
            interactable = null;
        }
    }
}

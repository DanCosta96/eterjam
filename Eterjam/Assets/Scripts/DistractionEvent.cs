﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistractionEvent : MonoBehaviour, Interactable
{

    public bool IsActive = true;
    public Action<DistractionEvent> returnOnActivate;

    public void Initialize(Action<DistractionEvent> doOnActivate)
    {
        if (doOnActivate != null)
         returnOnActivate = doOnActivate;
    }

    public void Interact()
    {
        UseDistraction();
    }
    public void UseDistraction()
    {
        returnOnActivate(this);
    }

    public void Disable()
    {
        IsActive = false;
    }
}

﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Room : MonoBehaviour
{
    public bool playerIsInRoom;
    public List<Vector3> Coordinates;

    public List<ParanormalEvent> ParanormalEvents;
    public List<DistractionEvent> DistractionEvents;

    private List<ParanormalEvent> eventsClone;
    private List<DistractionEvent> distractionClone;
    private BuyerController buyer;


    private void Awake()
    {
    }
    // Start is called before the first frame update
    void Start()
    {
        eventsClone = ParanormalEvents.ToArray().ToList();
        distractionClone = DistractionEvents.ToArray().ToList();
        foreach (var e in distractionClone)
        {
            e.Initialize(OnDistractionActivate);
        }

        for (int i = 0; i < 3; i++)
        {
            Coordinates.Add(GetRandomCoordinates(this.transform.position.y));
        }
    }

    public void OnDistractionActivate(DistractionEvent distraction)
    {
        if (BuyerIsInRoom())
        {
            if (distraction.IsActive)
            {
                buyer.OnDistraction(distraction.transform.position);
                distractionClone.Remove(distraction);
                distraction.Disable();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {


    }

    public ParanormalEvent RandomEventPicker()
    {
        var picked = eventsClone[UnityEngine.Random.Range(0, eventsClone.Count)];
        eventsClone.Remove(picked);
        return picked;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<BuyerController>() != null)
        {
            buyer = other.GetComponent<BuyerController>();
            var eventIsActive = false;
            foreach (var ev in ParanormalEvents)
            {
                if (ev.IsActive)
                {
                    eventIsActive = true;
                }
            }
            if (eventIsActive)
            {
                buyer.OnGettingScared();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<BuyerController>() != null)
        {
            buyer = null;
        }
    }

    public bool BuyerIsInRoom()
    {
        return buyer != null;
    }

    public int AvailableEventsCount()
    {
        return eventsClone.Count;
    }

    private Vector3 GetRandomCoordinates(float y)
    {
        var bounds = GetComponent<BoxCollider>().bounds.extents;
        return new Vector3(
            UnityEngine.Random.Range(bounds.x, -bounds.x),
            y,
            UnityEngine.Random.Range(bounds.z, -bounds.z));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputReader : MonoBehaviour
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	public Vector2 LeftStick()
    {
        float xAxis = Input.GetAxisRaw("Horizontal");
        float yAxis = Input.GetAxisRaw("Vertical");

        Vector2 v2 = new Vector2(xAxis, yAxis).normalized;
        return v2;
	}

    public Vector3 LeftStickYPlane()
    {
        float xAxis = Input.GetAxisRaw("Horizontal");
        float zAxis = Input.GetAxisRaw("Vertical");

        Vector3 v3 = new Vector3(xAxis, 0,zAxis).normalized;
        return v3;
    }

    public bool ActionButton()
    {
        return Input.GetButtonDown("Action");
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class Manager : MonoBehaviour
    {
        static public Manager Inst;
        public SceneHelper helper;
        public BuyerController buyer;
        private float TimeToEvent;
        public float MinTime;
        public float MaxTime;
        public Transform ExitPoint;
        private float timer;
        private List<Room> Rooms;
        private List<Room> roomsClone;

        private bool winCondition = false;

        private void Awake()
        {
            if (Inst == null)
                Inst = this;
            GetRoomsList();

        }
        void Start()
        {
            TimeToEvent = GenerateRandomInterval();
            roomsClone = Rooms.ToArray().ToList();
        }

        private void GetRoomsList()
        {
            Rooms = GetComponentsInChildren<Room>().ToList();
        }

        void Update()
        {
            timer += Time.deltaTime;
            if (timer >= TimeToEvent)
            {
                ResetTimer();
                if (roomsClone.Count > 0)
                {
                    var room = RandomRoomPicker();
                    if (room != null)
                    {
                        if (room.AvailableEventsCount() != 0)
                        {
                            room.RandomEventPicker().EnableSpookyness();
                            buyer.OnSpookyEvent(room.transform.position);
                        }
                    }
                }

                else
                {
                    winCondition = true;
                }
            }
        }

        private void ResetTimer()
        {
            timer = 0;
            TimeToEvent = GenerateRandomInterval();
        }

        private Room RandomRoomPicker()
        {
            var picked = roomsClone[UnityEngine.Random.Range(0, roomsClone.Count)];
            while (picked.BuyerIsInRoom())
            {
                if (roomsClone.Count == 1)
                {
                    return null;
                }
                picked = roomsClone[UnityEngine.Random.Range(0, roomsClone.Count)];
            }
            if (picked.AvailableEventsCount() <= 1)
            {
                roomsClone.Remove(picked);
            }
            return picked;
        }

        public Vector3 RandomRoomPosition()
        {
            var picked = Rooms[UnityEngine.Random.Range(0, Rooms.Count)];
            return picked.transform.position;
        }

        private float GenerateRandomInterval()
        {
            return UnityEngine.Random.Range(MinTime, MaxTime);
        }

        internal void EndGame()
        {
            if (winCondition)
                helper.GoToWin();
            else
                helper.GoToLose();
        }
    }
}

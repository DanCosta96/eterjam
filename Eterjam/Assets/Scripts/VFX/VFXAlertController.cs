﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXAlertController : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem AlertFX;
    [SerializeField]
    private Material QuestionMat;
    [SerializeField]
    private Color QuestionColor;
    [SerializeField]
    private Material ExclamationMat;
    [SerializeField]
    private Color ExclamationColor;

    [SerializeField]
    private Material DistractionMat;
    [SerializeField]
    private Color DistractionColor;

    public void DoQuestionAlert()
    {
        AlertFX.Stop();
        var renderer = AlertFX.GetComponent<ParticleSystemRenderer>();
       renderer.material = QuestionMat;
        var main = AlertFX.main;
        main.startColor = QuestionColor;
        AlertFX.Play();
    }

    public void DoDistractionAlert()
    {
        AlertFX.Stop();
        var renderer = AlertFX.GetComponent<ParticleSystemRenderer>();
        renderer.material = DistractionMat;
        var main = AlertFX.main;
        main.startColor = DistractionColor;
        AlertFX.Play();
    }

    public void DoExclamationAlert()
    {
        AlertFX.Stop();
        var renderer = AlertFX.GetComponent<ParticleSystemRenderer>();
        renderer.material = ExclamationMat;
        var main = AlertFX.main;
        main.startColor = ExclamationColor;
        AlertFX.Play();
    }
}

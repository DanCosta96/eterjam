﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHelper : MonoBehaviour
{
    public string MenuScene;
    public string GameScene;
    public string WinScene;
    public string LoseScene;

    public void GoToMenu()
    {
        SceneManager.LoadScene(MenuScene);
    }

    public void GoToGame()
    {
        SceneManager.LoadScene(GameScene);
    }

    public void GoToWin()
    {
        SceneManager.LoadScene(WinScene);
    }

    public void GoToLose()
    {
        SceneManager.LoadScene(LoseScene);
    }
}

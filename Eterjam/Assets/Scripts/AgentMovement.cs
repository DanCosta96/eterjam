﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class AgentMovement : MonoBehaviour {

    private NavMeshAgent agent;
    private Action doOnDestination;
    // Use this for initialization
    void Awake () {
        agent = GetComponent<NavMeshAgent>();
    }
    private void Update()
    {
        if (Vector3.Distance(agent.destination,transform.position) <= 1)
        {
            if (doOnDestination != null)
            {
                doOnDestination();
                doOnDestination = null;
            }
        }
    }

    public void Move (Vector3 aDir)
    {
        agent.Move(aDir);
    }

    public void GoTo(Vector3 destination)
    {
        agent.SetDestination(destination);
    }

    public void GoTo(Vector3 destination, Action doOnReach)
    {
        doOnDestination = doOnReach;
        agent.SetDestination(destination);
    }

    public void Stop()
    {
        agent.SetDestination(transform.position);
    }

}

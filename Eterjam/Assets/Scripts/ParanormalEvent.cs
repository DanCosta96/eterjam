﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class ParanormalEvent : MonoBehaviour, Interactable
    {
        public bool IsActive;
        public Animator Animation;
        public AudioSource sfx;

        public void EnableSpookyness()
        {
            IsActive = true;
            Debug.Log("Se activo evento de " + gameObject.name);
            Animation.SetBool("IsSpookying", true);
            sfx.Play();
        }

        public void DisableSpookyness()
        {
            IsActive = false;
            Debug.Log("Se desactivo evento de " + gameObject.name);
            Animation.SetBool("IsSpookying", false);
            sfx.Stop();
        }
        
        public void Interact()
        {
            if (IsActive)
            {
                DisableSpookyness();
            }
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (IsActive)
                {
                    DisableSpookyness();
                }
                else
                {
                    EnableSpookyness();
                }
            }
        }
    }
}

﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyerController : MonoBehaviour
{
    public AgentMovement agent;

    public IEnumerator waitAndGoToARandomRoom;
    public IEnumerator waitAndGoToCheckEvent;
    public IEnumerator waitAndGoToCheckDistraction;

    public VFXAlertController alert;
    public AudioSource sfxScared;
    public AudioSource sfxSpooky;

    public bool ExitHouseAction = false;

    private Vector3 LastSpookyEventPosition;
    private Vector3 LastRoomCheckPosition;

    void Start()
    {
        LastRoomCheckPosition = RandomRoom;
        agent.GoTo(LastRoomCheckPosition, WaitAndGoToARandomRoom);
    }
    
    private Vector3 Exit => Manager.Inst.ExitPoint.position;

    private Vector3 RandomRoom => Manager.Inst.RandomRoomPosition();

    public void OnGettingScared()
    {
        ExitHouseAction = true;
        agent.Stop();
        StopAllCoroutines();
        alert.DoExclamationAlert();
        sfxScared.Play();
        agent.GoTo(Exit, ExitReached);
    }

    public void GoToExit()
    {
        ExitHouseAction = true;
        agent.Stop();
        StopAllCoroutines();
        alert.DoExclamationAlert();
        agent.GoTo(Exit, ExitReached);
    }


    private void WaitAndGoToARandomRoom()
    {
        waitAndGoToARandomRoom = WaitForSecondsAndThen(5f, GoToARandomRoom);
        StartCoroutine(waitAndGoToARandomRoom);
    }

    private void GoToARandomRoom()
    {
        if (ExitHouseAction)
            return;
        LastRoomCheckPosition = RandomRoom;
        agent.GoTo(LastRoomCheckPosition, WaitAndGoToARandomRoom);
    }

    public void OnSpookyEvent(Vector3 spookyPosition)
    {
        if (ExitHouseAction)
            return;
        agent.Stop();
        StopCoroutine(waitAndGoToARandomRoom);
        alert.DoQuestionAlert();
        sfxSpooky.Play();
        LastSpookyEventPosition = spookyPosition;
        WaitAndGoToSpookyRoom();
    }

    private void WaitAndGoToSpookyRoom()
    {
        waitAndGoToCheckEvent = WaitForSecondsAndThen(2f, GoToASpookyRoom);
        StartCoroutine(waitAndGoToCheckEvent);
    }

    private void GoToASpookyRoom()
    {
        agent.GoTo(LastSpookyEventPosition, WaitAndGoToARandomRoom);
    }

    public void OnDistraction(Vector3 spookyPosition)
    {

        if (ExitHouseAction)
            return;
        StopAllCoroutines();
        agent.Stop();
        alert.DoDistractionAlert();
        LastSpookyEventPosition = spookyPosition;
        WaitAndGoToSpookyRoom();
    }

    private void WaitAndGoToCheckDistraction()
    {
        waitAndGoToCheckEvent = WaitForSecondsAndThen(.5f, GoToCheckDistraction);
        StartCoroutine(waitAndGoToCheckEvent);
    }

    private void GoToCheckDistraction()
    {
        agent.GoTo(LastSpookyEventPosition, WaitAndGoToSpookyRoom);
    }

    private void ExitReached()
    {
        Manager.Inst.EndGame();
    }

    IEnumerator WaitForSecondsAndThen(float secondsToWait, Action onTimeEnded )
    {
        yield return new WaitForSeconds(secondsToWait);
        onTimeEnded();
    }
}

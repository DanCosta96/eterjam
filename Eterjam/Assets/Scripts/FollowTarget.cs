﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    public Transform Target;
    public bool Lerp = false;
    public float LerpSpeed;
    private Vector3 offset = Vector3.zero;
    
    void Start()
    {
        offset = transform.position - Target.position;
    }
    
    void LateUpdate()
    {
        if (!Lerp)
            transform.position = Target.position + offset;
        else
            transform.position = Vector3.Lerp(transform.position, Target.position + offset, Time.deltaTime * LerpSpeed);
    }
}
